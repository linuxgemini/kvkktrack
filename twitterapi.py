import config
import re
from time import sleep
import tweepy

def _build_api():
    if (
        len(config.twitter_bearer_token) > 0
        and len(config.twitter_consumer_api_key) > 0
        and len(config.twitter_consumer_api_secret_key) > 0
        and len(config.twitter_access_token) > 0
        and len(config.twitter_access_token_secret) > 0
    ):
        return tweepy.Client(
            bearer_token=config.twitter_bearer_token,
            consumer_key=config.twitter_consumer_api_key,
            consumer_secret=config.twitter_consumer_api_secret_key,
            access_token=config.twitter_access_token,
            access_token_secret=config.twitter_access_token_secret
        )
    else:
        return False

def send_tweet(kvkk_urls):
    extractedURLs = {}

    for u in kvkk_urls:
        konu = re.sub(r"(https:\/\/www\.kvkk\.gov\.tr\/Icerik\/)(\d+)(\/Kamuoyu-Duyurusu(-Veri)?-Ihlali?-Bildirimi)-(\S*)", r"\5", u)
        icerikno = re.sub(r"(https:\/\/www\.kvkk\.gov\.tr\/Icerik\/)(\d+)(\/Kamuoyu-Duyurusu(-Veri)?-Ihlali?-Bildirimi)-(\S*)", r"\2", u)
        if (konu != "" and len(konu) > 0):
            extractedURLs[konu] = u
        else:
            extractedURLs[icerikno] = u

    twapi = _build_api()

    if (twapi != False):
        for urlname, url in extractedURLs.items():
            twh = f"{urlname}".replace("-", " ").strip()
            _send_tweet(twapi, f"{twh}\n\n{url}")

def _send_tweet(api, text):
    api.create_tweet(text=text, user_auth=True)
    sleep(0.5)
