# kvkktrack

Tracking KVKK releases automatically because nobody does it.

## Installation
- Install python3.6+
- Install the requirements in `requirements.txt`
- Copy `config.template.py` to `config.py`
- Configure `config.py` with your Discord webhook URL
- Run `kvkktrack.py`. You may want to set up a cronjob or systemd timer for auto-posting updates.

One thing to note: The initial run will *not* send a webhook.

## Credits I guess?

Yes I shamelessly stole some of [Ave's](https://gitlab.com/a) work
