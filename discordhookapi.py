import re
import config
import requests

def send_webhook(kvkk_urls):
    embeds = []

    embedobject = {
        "author": {
            "name": "KVKK Kuryesi",
            "icon_url": "https://btw.i-use-ar.ch/i/wrsw2rb9.png"
        },
        "title": f"{len(kvkk_urls)} yeni duyuru",
        "description": ""
    }

    for u in kvkk_urls:
        konu = re.sub(r"(https:\/\/www\.kvkk\.gov\.tr\/Icerik\/)(\d+)(\/Kamuoyu-Duyurusu(-Veri)?-Ihlali?-Bildirimi)-(\S*)", r"\5", u)
        icerikno = re.sub(r"(https:\/\/www\.kvkk\.gov\.tr\/Icerik\/)(\d+)(\/Kamuoyu-Duyurusu(-Veri)?-Ihlali?-Bildirimi)-(\S*)", r"\2", u)
        if konu != "":
            embedobject["description"] += f"[{konu}]({u})\n"
        else:
            embedobject["description"] += f"[{icerikno}]({u})\n"

    embeds.append(embedobject)

    if len(config.discord_hook_url) > 0:
        _send_webhook("", embeds)

def _send_webhook(text, embeds):
    o = requests.post(config.discord_hook_url, json={"content": text, "embeds": embeds})
    if o.status_code != 204:
        return print("Well, discord gave up!")
