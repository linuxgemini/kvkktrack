#!/usr/bin/env python3

from bs4 import BeautifulSoup
import discordhookapi
import json
import os
import re
import requests
import twitterapi

if os.path.exists("state.json"):
    with open("state.json") as f:
        old_data = json.load(f)
else:
    old_data = []

new_data = []

def main():
    out = requests.get("https://kvkk.gov.tr/sitemap.xml", verify=False)

    if out.status_code != 200:
        return print("Something is not right on KVKK!")

    soup = BeautifulSoup(out.text, "xml")

    for bug in soup.find_all("url"):
        strin = re.match(r"(https:\/\/www\.kvkk\.gov\.tr\/Icerik\/)(\d+)(\/Kamuoyu-Duyurusu(-Veri)?-Ihlali?-Bildirimi)-(\S*)", bug.loc.string)

        if strin:
            new_data.append(strin.string)

    deduped_new_data = list(set(new_data))  # insta dedupe i hope
    deduped_new_data.sort()                 # sorts perfectly(tm)

    if deduped_new_data != old_data:
        print("New data!")
        if old_data != []:
            print("Gathering diff")
            diff = list(frozenset(deduped_new_data).difference(old_data))
            diff.sort()                     # WHY

            print("Passed to Discord!")
            discordhookapi.send_webhook(diff)

            print("Passed to Twitter!")
            twitterapi.send_tweet(diff)

        with open("state.json", "w") as f:
            json.dump(deduped_new_data, f)
            print("Data saved to state file.")

    return

main()
